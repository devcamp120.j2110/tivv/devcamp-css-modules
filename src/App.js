import avatar from './assets/images/48.jpg'
import AppStyle from './App.module.css'

function App() {
  return (
    <div className= {AppStyle.devCamp}>
      <div className={AppStyle.devcampWrapper}><img alt='avatar' className={AppStyle.devcampAvatar} src={avatar} />
        <div>
          <p  className={AppStyle.devcampQuoteP}>This is one of the best developer blogs on the plannet. I read it daily to improve my skills </p>
        </div>
        <p className={AppStyle.devcampName}>Tammy Stevens.<span className={AppStyle.devcampNameSpan}>- Front end developer</span></p></div>
    </div>
  );
}

export default App;
